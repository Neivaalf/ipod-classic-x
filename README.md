# iPod Classic X

First, a bit of context:

- I love 2000s tech
- I upgraded my phone and didn't know what to do with my spare
- I listened to Spotify Playlists, but never had the opportunity to explicitly like songs, or check who is the artist, what's the album name or cover

This project aims to create a Spotify Player with an iPod interface. It's made to run on my spare phone that I can use as a docked player on my desk.

The accent color is picked from the album. It supports light and dark mode.

The main technical challenge was to handle the volume. Working with spinning circles forces me to dig my old trigonometry courses.

Try yourself here: [https://neivaalf.gitlab.io/ipod-classic-x/player](https://neivaalf.gitlab.io/ipod-classic-x/player)

## Result

![alt text](image-5.png)

![alt text](image-4.png)

## Inspirations / Mood Board

![alt text](image-2.png)

![alt text](image.png)

![alt text](image-1.png)

![alt text](image-3.png)
