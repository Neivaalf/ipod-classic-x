import React from "react"
import { useHistory } from "react-router-dom"

import { PlayerContext } from "components/Player"

import useObjectFit from "hooks/useObjectFit"
import useSpotify from "hooks/useSpotify"

import Icon from "components/Icon"

import styles from "./ClickWheel.module.scss"

type Coordinates = {
  x: number
  y: number
}

const getRelativeCoordinates = (
  center: Coordinates,
  point: Coordinates
): Coordinates => ({
  x: point.x - center.x,
  y: point.y - center.y
})

const CLICKS_PER_TURN = 128
const ANGLE_OF_ONE_CLICK = 360 / CLICKS_PER_TURN

const radToDeg = (radian: number) => radian / (Math.PI / 180)

const getAngle = (center: Coordinates, point: Coordinates) => {
  const relativePoint = getRelativeCoordinates(center, point)

  return Math.atan2(relativePoint.y, relativePoint.x)
}

const ClickWheel = ({ className, size }) => {
  const history = useHistory()
  const spotifyFetch = useSpotify()

  const clickWheelRef = React.useRef<HTMLDivElement>(null)

  const { player } = React.useContext(PlayerContext)

  const [isPressed, setIsPressed] = React.useState(false)

  const [CWCenter, setCWCenter] = React.useState<Coordinates>({
    x: 0,
    y: 0
  })

  const [CWPressed, setCWPressed] = React.useState<Coordinates>({
    x: 0,
    y: 0
  })

  const [CWCurrent, setCWCurrent] = React.useState<Coordinates>({
    x: 0,
    y: 0
  })

  const [counter, setCounter] = React.useState<number>(0)

  const [isPlaying, setIsPlaying] = React.useState(player?.is_playing)

  React.useEffect(() => {
    setIsPlaying(player?.is_playing)

    if (!isPressed) {
      setCounter(0)
    }
  }, [player, isPressed])

  React.useLayoutEffect(() => {
    if (clickWheelRef.current) {
      const boundingClientRect = clickWheelRef.current.getBoundingClientRect()

      setCWCenter({
        x: (boundingClientRect.right + boundingClientRect.left) / 2,
        y: (boundingClientRect.bottom + boundingClientRect.top) / 2
      })
    }
  }, [clickWheelRef, size])

  React.useEffect(() => {
    const CWCurrentAngle = getAngle(CWCenter, CWCurrent)
    const CWPressedAngle = getAngle(CWCenter, CWPressed)

    const tmpAngle = CWPressedAngle - CWCurrentAngle
    const K = CWPressedAngle > CWCurrentAngle ? Math.PI * -2 : Math.PI * 2

    const angleRad =
      Math.abs(K + tmpAngle) < Math.abs(tmpAngle) ? K + tmpAngle : tmpAngle

    const angleDeg = radToDeg(angleRad) * -1

    if (Math.abs(angleDeg) > ANGLE_OF_ONE_CLICK) {
      setCWPressed(CWCurrent)

      if (angleDeg > 0) {
        const incrementBy = Math.floor(angleDeg / ANGLE_OF_ONE_CLICK)
        setCounter((c) => c + incrementBy)
      }
      if (angleDeg < 0) {
        const incrementBy = Math.ceil(angleDeg / ANGLE_OF_ONE_CLICK)
        setCounter((c) => c + incrementBy)
      }
    }
  }, [CWCenter, CWPressed, CWCurrent])

  return (
    <div
      ref={clickWheelRef}
      onPointerDown={(e) => {
        // console.log(e.target)
        setIsPressed(true)
        setCWPressed({
          x: e.clientX,
          y: e.clientY
        })
        setCWCurrent({
          x: e.clientX,
          y: e.clientY
        })
      }}
      onPointerUp={(e) => {
        // console.log(e.target)
        setIsPressed(false)

        const currentVolumePercent = player?.device?.volume_percent ?? 100
        const newVolume = Math.max(
          0,
          Math.min(currentVolumePercent + counter, 100)
        )

        if (newVolume !== currentVolumePercent) {
          spotifyFetch(
            `https://api.spotify.com/v1/me/player/volume?volume_percent=${newVolume}`,
            {
              method: "PUT"
            }
          )
        }
      }}
      onPointerMove={(e) => {
        // console.log(CWCenter)

        if (isPressed) {
          setCWCurrent({
            x: e.clientX,
            y: e.clientY
          })
        }
      }}
      className={[className, styles.ClickWheel].join(" ")}
      style={{
        height: `${size}px`,
        width: `${size}px`
      }}
    >
      <div
        className={styles.menu}
        onClick={() => {
          history.push("/playlists")
        }}
      >
        menu
      </div>
      <div
        onClick={() => {
          spotifyFetch("https://api.spotify.com/v1/me/player/previous", {
            method: "POST"
          })
        }}
      >
        <Icon className={styles.previous}>fast_rewind</Icon>
      </div>
      <div
        onClick={() => {
          spotifyFetch("https://api.spotify.com/v1/me/player/next", {
            method: "POST"
          })
        }}
      >
        <Icon className={styles.next}>fast_forward</Icon>
      </div>
      <div
        className={styles.play}
        onClick={() => {
          if (isPlaying) {
            spotifyFetch("https://api.spotify.com/v1/me/player/pause", {
              method: "PUT"
            })
          }
          if (!isPlaying) {
            spotifyFetch("https://api.spotify.com/v1/me/player/play", {
              method: "PUT"
            })
          }
        }}
      >
        <Icon>play_arrow</Icon>
        <Icon>pause</Icon>
      </div>
      <div
        className={styles.innerCircle}
        onPointerDown={(e) => {
          // console.log(e.target)
          e.stopPropagation()
        }}
        onPointerMove={(e) => {
          e.stopPropagation()
        }}
        onPointerUp={(e) => {
          // console.log(e.target)
          e.stopPropagation()
        }}
      >
        {/* {counter} {isPressed ? "P" : "NP"} */}
      </div>
    </div>
  )
}

export default (props) => {
  const ClickWheelWithSize = useObjectFit(ClickWheel)

  return <ClickWheelWithSize {...props} />
}
