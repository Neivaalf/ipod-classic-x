import React from "react"

import styles from "./Header.module.scss"

import { ThemeContext } from "components/Ipod"

export default ({ children }) => {
  const [theme, setTheme] = React.useContext(ThemeContext)

  return (
    <div
      className={styles.Header}
      onClick={() => {
        setTheme(theme === "light" ? "dark" : "light")
      }}
    >
      {children}
    </div>
  )
}
