import React from "react"

import { PlayerContext } from "components/Player"

import InfiniteText from "components/InfiniteText"

import styles from "./Screen.module.scss"

export default ({ className }) => {
  const { player } = React.useContext(PlayerContext)
  const [data, setData] = React.useState<any>({}) // FIXME: Type it properly

  const songId = player?.item?.id
  React.useEffect(() => {
    setData({
      image: player?.item?.album?.images?.["1"],
      song: player?.item?.name,
      artists: player?.item?.artists.map((artist) => artist.name),
      album: player?.item?.album?.name,
      totalTracks: player?.item?.album?.total_tracks,
      trackNumber: player?.item?.track_number
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [songId])

  return (
    <div className={[className, styles.Screen].join(" ")}>
      <img
        src={data?.image?.url}
        alt={`${data?.song} - ${data?.artists?.join(", ")}`}
        className={styles.albumCover}
      />
      <div className={styles.song}>
        <InfiniteText>{data?.song}</InfiniteText>
      </div>
      <div className={styles.album}>
        <InfiniteText>{data?.album}</InfiniteText>
      </div>

      <div className={styles.artists}>
        <InfiniteText>{data?.artists?.join(", ")}</InfiniteText>
      </div>

      <div className={styles.trackNumber}>
        {data?.trackNumber} of {data?.totalTracks}
      </div>
    </div>
  )
}
