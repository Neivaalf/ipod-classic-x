import React, { ReactNode } from "react"

export default ({
  children,
  className,
  ...props
}: {
  children: ReactNode
  className?: string
}) => (
  <i className={["material-icons-round", className].join(" ")} {...props}>
    {children}
  </i>
)
