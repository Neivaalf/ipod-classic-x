import React from "react"
import { useHistory } from "react-router-dom"

import useSpotify from "hooks/useSpotify"

import Header from "components/Header"

import styles from "./Playlists.module.scss"

export default () => {
  const spotifyFetch = useSpotify()
  const history = useHistory()

  const [playlists, setPlaylists] = React.useState<any[]>([])

  React.useEffect(() => {
    ;(async () => {
      const res = await spotifyFetch(
        "https://api.spotify.com/v1/me/playlists?limit=50"
      )

      setPlaylists(res.items)
    })()
  }, [spotifyFetch])

  return (
    <div className={styles.Playlist}>
      <Header>Playlists</Header>

      <div className={styles.playlists}>
        {playlists.map((playlist) => (
          <div
            className={styles.playlist}
            key={playlist.id}
            onClick={async () => {
              await spotifyFetch("https://api.spotify.com/v1/me/player/play", {
                method: "PUT",
                body: JSON.stringify({
                  context_uri: playlist.uri
                })
              })

              history.push("/player")
            }}
          >
            <img
              className={styles.image}
              alt={playlist.name}
              src={playlist?.images?.[0]?.url}
            />
            {playlist.name}
          </div>
        ))}
      </div>
    </div>
  )
}
