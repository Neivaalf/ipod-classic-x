import React from "react"

import Header from "components/Header"

import { PlayerContext } from "components/Player"

import useSpotify from "hooks/useSpotify"

export default () => {
  const spotifyFetch = useSpotify()

  const { player } = React.useContext(PlayerContext)
  const playlistId = player?.context?.uri.split(":").pop()

  const [playlist, setPlaylist] = React.useState<{ name: string }>()

  React.useEffect(() => {
    ;(async () => {
      if (playlistId) {
        const res = await spotifyFetch(
          `https://api.spotify.com/v1/playlists/${playlistId}`
        )

        setPlaylist({
          name: res?.name
        })
      }
    })()
  }, [playlistId, spotifyFetch])

  return playlist?.name ? (
    <Header>{playlist.name}</Header>
  ) : (
    <Header>Library</Header>
  )
}
