import React from "react"
import { Redirect } from "react-router-dom"

import styles from "./Login.module.scss"

import { AuthorizationContext } from "components/Ipod"
const parseQueryParams = (search: string) => {
  const queryParamsObject: { [key: string]: string } = {}

  // Remove the first character (# or ?)
  const queryParamsTable = search.substring(1).split("&")

  queryParamsTable.forEach((queryParam) => {
    const [key, value] = queryParam.split("=")
    queryParamsObject[key] = value
  })
  return queryParamsObject
}

const objectToFormUrlEncoded = (body) =>
  Object.keys(body)
    .map((key) => `${key}=${body[key]}`)
    .join("&")

export default () => {
  const [accessToken, setAccessToken] = React.useContext(AuthorizationContext)

  const [code, setCode] = React.useState("")

  React.useEffect(() => {
    const { code } = parseQueryParams(window.location.search)
    setCode(code)

    const getAccessTokenFromCode = async () => {
      const res = await fetch(
        `${process.env.REACT_APP_SPOTIFY_PROXY}/api/token`,
        {
          method: "POST",
          body: JSON.stringify({
            grant_type: "authorization_code",
            redirect_uri: window.location.origin + window.location.pathname,
            code
          })
        }
      )

      if (res.status === 200) {
        const {
          refresh_token: refreshToken,
          access_token: accessToken
        } = await res.json()

        localStorage.setItem("REFRESH_TOKEN", refreshToken)
        setAccessToken(accessToken)
      }
      // FIXME: Should be handled with react router
      else {
        window.location.assign(
          window.location.origin + window.location.pathname
        )
      }
    }

    if (code) {
      getAccessTokenFromCode()
    }

    const refreshToken = localStorage.getItem("REFRESH_TOKEN")

    const getAccessTokenFromRefreshToken = async () => {
      const res = await fetch(
        `${process.env.REACT_APP_SPOTIFY_PROXY}/api/token`,
        {
          method: "POST",
          body: JSON.stringify({
            grant_type: "refresh_token",
            refresh_token: refreshToken
          })
        }
      )
      if (res.status === 200) {
        const { access_token: accessToken } = await res.json()
        setAccessToken(accessToken)
      }
      // FIXME: Should be handled with react router
      else {
        window.location.assign(
          window.location.origin + window.location.pathname
        )
      }
    }

    if (refreshToken) {
      getAccessTokenFromRefreshToken()
    }
  }, [setAccessToken])

  const params = objectToFormUrlEncoded({
    client_id: process.env.REACT_APP_CLIENT_ID,
    response_type: "code",
    redirect_uri: window.location.origin + window.location.pathname,
    scope: encodeURIComponent(
      [
        "user-read-playback-state",
        "user-modify-playback-state",
        "user-library-read",
        "user-library-modify",
        "user-read-recently-played",
        "playlist-read-private",
        "playlist-read-collaborative"
      ].join(" ")
    )
  })

  return (
    <div className={styles.Login}>
      {accessToken && <Redirect to={{ pathname: "/player" }} />}
      {code ? (
        <div className={styles.loginButton}>Logging in...</div>
      ) : (
        <a
          href={`https://accounts.spotify.com/authorize?${params}`}
          className={styles.loginButton}
        >
          Login with Spotify
        </a>
      )}
    </div>
  )
}
