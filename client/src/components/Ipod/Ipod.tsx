import React from "react"
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"

import styles from "./Ipod.module.scss"

import useTheme from "hooks/useTheme"

import Player from "components/Player"
import Playlists from "components/Playlists"
import Login from "components/Login"

export const AuthorizationContext = React.createContext<
  [string, React.Dispatch<React.SetStateAction<string>>]
>(["", () => null])

export const ThemeContext = React.createContext<
  [string, React.Dispatch<React.SetStateAction<string>>]
>(["dark", () => null])

export default () => {
  const [theme, setTheme] = useTheme("dark")
  const [accessToken, setAccessToken] = React.useState("")

  return (
    <AuthorizationContext.Provider value={[accessToken, setAccessToken]}>
      <ThemeContext.Provider value={[theme, setTheme]}>
        <BrowserRouter basename={"/ipod-classic-x"}>
          <div className={styles.Ipod}>
            <Switch>
              {!accessToken && (
                <>
                  <Route path="/login">
                    <Login />
                  </Route>
                  <Route path="*">
                    <Redirect
                      to={{
                        pathname: "/login",
                        search: window.location.search
                      }}
                    />
                  </Route>
                </>
              )}
              <Route path="/player">
                <Player />
              </Route>
              <Route path="/playlists">
                <Playlists />
              </Route>
              <Route path="/login">
                <Login />
              </Route>
              <Route path="*">
                <Redirect to="/login" />
              </Route>
            </Switch>
          </div>
        </BrowserRouter>
      </ThemeContext.Provider>
    </AuthorizationContext.Provider>
  )
}
