import React from "react"

import styles from "./InfiniteText.module.scss"

export default ({ children }) => {
  const [containerWidth, setContainerWidth] = React.useState(0)
  const [textWidth, setTextWidth] = React.useState(0)

  const [containerEl, setContainerEl] = React.useState<Element>()
  const [childEl, setChildEl] = React.useState<Element>()

  const containerRef = React.useCallback(node => {
    if (node !== null) {
      setContainerEl(node)
      setContainerWidth(node.clientWidth)
    }
  }, [])

  const childRef = React.useCallback(
    node => {
      if (node !== null) {
        setChildEl(node)
        setTextWidth(node.scrollWidth)
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [children]
  )

  React.useEffect(() => {
    if (containerEl && childEl) {
      const updateSizes = () => {
        setContainerWidth(containerEl.clientWidth)
        setTextWidth(childEl.scrollWidth)
      }
      window.addEventListener("resize", updateSizes)

      return () => {
        window.removeEventListener("resize", updateSizes)
      }
    }
  }, [containerEl, childEl])

  const doesTextFitContainer = textWidth < containerWidth

  return (
    <div
      className={[
        styles.InfiniteText,
        doesTextFitContainer ? "" : styles.animated
      ].join(" ")}
      ref={containerRef}
    >
      <div ref={childRef} className={styles.text}>
        {children}
      </div>

      {!doesTextFitContainer && <div className={styles.text}>{children}</div>}
    </div>
  )
}
