import React from "react"
import { format, addMilliseconds } from "date-fns"

import { PlayerContext } from "components/Player"

import useSpotify from "hooks/useSpotify"

import Icon from "components/Icon"

import styles from "./ProgressBar.module.scss"

export default () => {
  const spotifyFetch = useSpotify()
  const { player, updatePlayer } = React.useContext(PlayerContext)

  const [data, setData] = React.useState<any>({}) // FIXME: Type it properly

  const formatTime = (timeInMs: number) => {
    return format(addMilliseconds(new Date(0), timeInMs), "m:ss")
  }

  React.useEffect(() => {
    ;(async () => {
      let res
      if (player?.item?.id) {
        res = await spotifyFetch(
          `https://api.spotify.com/v1/me/tracks/contains?ids=${player.item.id}`
        )
      }

      const [isLiked] = res ?? [false]

      setData({
        shuffleState: player?.shuffle_state,
        repeatState: player?.repeat_state,
        progress: player?.progress_ms ? formatTime(player.progress_ms) : "0:00",
        progressLeft:
          player?.progress_ms && player?.item?.duration_ms
            ? `-${formatTime(player.item.duration_ms - player.progress_ms)}`
            : "-0:00",
        progressPercent:
          (player?.progress_ms / player?.item?.duration_ms) * 100,
        isLiked
      })
    })()
  }, [player, spotifyFetch])

  const songId = player?.item?.id

  return (
    <div className={styles.ProgressBar}>
      <div className={styles.modes}>
        <div
          className={[styles.icon, data.shuffleState ? styles.active : ""].join(
            " "
          )}
          onClick={async () => {
            await spotifyFetch(
              `https://api.spotify.com/v1/me/player/shuffle?state=${!data.shuffleState}`,
              {
                method: "PUT"
              }
            )
            updatePlayer()
          }}
        >
          <Icon>shuffle</Icon>
        </div>
        <div
          className={[styles.icon, data.isLiked ? styles.active : ""].join(" ")}
          onClick={async () => {
            if (!songId) return
            if (data.isLiked) {
              await spotifyFetch(
                `https://api.spotify.com/v1/me/tracks?ids=${songId}`,
                {
                  method: "DELETE"
                }
              )
            } else {
              await spotifyFetch(
                `https://api.spotify.com/v1/me/tracks?ids=${songId}`,
                {
                  method: "PUT"
                }
              )
            }

            const res = await spotifyFetch(
              `https://api.spotify.com/v1/me/tracks/contains?ids=${songId}`
            )

            const [isLiked] = res
            setData({ ...data, isLiked })
          }}
        >
          {data.isLiked ? <Icon>favorite</Icon> : <Icon>favorite_border</Icon>}
        </div>
        <div
          className={[
            styles.icon,
            data.repeatState === "context" || data.repeatState === "track"
              ? styles.active
              : ""
          ].join(" ")}
          onClick={async () => {
            let nextState
            if (data.repeatState === "off") nextState = "context"
            if (data.repeatState === "context") nextState = "track"
            if (data.repeatState === "track") nextState = "off"

            await spotifyFetch(
              `https://api.spotify.com/v1/me/player/repeat?state=${
                nextState ?? "off"
              }`,
              {
                method: "PUT"
              }
            )
            updatePlayer()
          }}
        >
          {data.repeatState === "track" ? (
            <Icon>repeat_one</Icon>
          ) : (
            <Icon>repeat</Icon>
          )}
        </div>
      </div>
      <div className={styles.barWrapper}>
        <div
          className={styles.bar}
          style={{
            transform: `translateX(-${100 - data.progressPercent}%)`
          }}
        ></div>
      </div>
      <div className={styles.timers}>
        <div>{data.progress}</div>
        <div>{data.progressLeft}</div>
      </div>
    </div>
  )
}
