import React from "react"

import Ipod from "components/Ipod"

import styles from "./App.module.scss"

export default () => (
  <main className={styles.main}>
    <Ipod />
  </main>
)
