import React from "react"
import Vibrant from "node-vibrant"
import tinycolor from "tinycolor2"

import { ThemeContext } from "components/Ipod"
import useSpotify from "hooks/useSpotify"

import Status from "components/Status"
import Screen from "components/Screen"
import ProgressBar from "components/ProgressBar"
import ClickWheel from "components/ClickWheel"

import styles from "./Player.module.scss"

import { getCssVar, setCssVar } from "hooks/useTheme"

export const PlayerContext = React.createContext<any>(null)

export default () => {
  const spotifyFetch = useSpotify()
  const [player, setPlayer] = React.useState<any>({})

  const [theme] = React.useContext(ThemeContext)

  const updatePlayer = React.useCallback(async () => {
    const res = await spotifyFetch("https://api.spotify.com/v1/me/player/")
    if (res) {
      setPlayer(res)
    } else {
      const tmp = await spotifyFetch(
        "https://api.spotify.com/v1/me/player/recently-played?limit=1"
      )
      setPlayer({
        item: tmp?.items?.[0]?.track
      })
    }
  }, [spotifyFetch])

  React.useEffect(() => {
    updatePlayer()
    const intervalId = setInterval(() => {
      updatePlayer()
    }, 1000)

    return () => {
      clearInterval(intervalId)
    }
  }, [setPlayer, updatePlayer])

  React.useEffect(() => {
    ;(async () => {
      const image = player?.item?.album?.images?.["1"]?.url

      if (image) {
        const colorPalette = await Vibrant.from(image).getPalette()

        const themeBackground = tinycolor(
          getCssVar("--ipodBackgroundColor")
        ).toHexString()

        const MAX_COLOR_DIFF = { color: "", diff: 100 }
        for (const [key, value] of Object.entries(colorPalette)) {
          if (value?.hex) {
            const diff = Vibrant.Util.hexDiff(themeBackground, value.hex)

            const biasedDiff = Math.abs(diff - 60)

            if (biasedDiff < MAX_COLOR_DIFF.diff) {
              MAX_COLOR_DIFF.color = key
              MAX_COLOR_DIFF.diff = biasedDiff
            }
          }
        }

        setCssVar("--accent", colorPalette?.[MAX_COLOR_DIFF.color]?.hex)
      }
    })()
  }, [theme, player])

  return (
    <PlayerContext.Provider value={{ player, updatePlayer }}>
      <div className={styles.Player}>
        <Status />
        <Screen className={styles.screen} />
        <ProgressBar />
        <ClickWheel className={styles.clickWheel} />
      </div>
    </PlayerContext.Provider>
  )
}
