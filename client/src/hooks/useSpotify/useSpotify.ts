import React from "react"
import { useHistory } from "react-router-dom"

import { AuthorizationContext } from "components/Ipod"

export default () => {
  const history = useHistory()
  const [accessToken, setAccessToken] = React.useContext(AuthorizationContext)

  const useSpotifyFetch = async (input: RequestInfo, init?: RequestInit) => {
    const res = await fetch(input, {
      ...init,
      headers: {
        ...init?.headers,
        Authorization: `Bearer ${accessToken}`
      }
    })

    if (res.status === 200) {
      try {
        return await res.json()
      } catch (e) {
        return {}
      }
    }

    if (res.status === 400 || res.status === 401) {
      setAccessToken("")
      history.push("/login")
    }
  }

  return React.useCallback(useSpotifyFetch, [accessToken])
}
