import React from "react"

import styles from "./useObjectFit.module.scss"

export default Component => {
  const WithObjectFit = props => {
    const objectFitRef = React.useRef<HTMLDivElement>(null)

    const [height, setHeight] = React.useState(0)
    const [width, setWidth] = React.useState(0)

    React.useEffect(() => {
      if (objectFitRef && objectFitRef.current) {
        const objectFitObserver = new ResizeObserver(entries => {
          entries.forEach(entry => {
            const { height, width } = entry.contentRect
            setHeight(height)
            setWidth(width)
          })
        })

        objectFitObserver.observe(objectFitRef.current)

        return () => {
          objectFitObserver.disconnect()
        }
      }
    }, [objectFitRef])

    return (
      <div ref={objectFitRef} className={styles.ObjectFit}>
        <Component {...props} size={Math.min(height, width)} />
      </div>
    )
  }

  return React.useCallback(WithObjectFit, [])
}
