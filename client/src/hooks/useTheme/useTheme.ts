import React from "react"

export const setCssVar = (name, value) => {
  document.documentElement.style.setProperty(name, value)
}

export const getCssVar = (name) =>
  getComputedStyle(document.documentElement).getPropertyValue(name)

export default (
  initialValue: string
): [string, React.Dispatch<React.SetStateAction<string>>] => {
  const [theme, setTheme] = React.useState(initialValue)

  React.useEffect(() => {
    switch (theme) {
      case "dark":
        setCssVar("--ipodBackgroundColor", getCssVar("--DARK_GREY"))
        setCssVar("--fontColor", getCssVar("--WHITE-80"))
        setCssVar("--clickWheelBackground", getCssVar("--CLICK_WHEEL_BG_DARK"))
        break
      default:
        setCssVar("--ipodBackgroundColor", getCssVar("--WHITE"))
        setCssVar("--fontColor", getCssVar("--BLACK-80"))
        setCssVar("--clickWheelBackground", getCssVar("--CLICK_WHEEL_BG_LIGHT"))
        break
    }
  }, [theme])

  return [theme, setTheme]
}
