import { VercelRequest, VercelResponse } from "@vercel/node"
import https from "https"

export default async (
  vercelRequest: VercelRequest,
  vercelResponse: VercelResponse
) => {
  const { CLIENT_SECRET_ID, CLIENT_ID } = process.env

  const objectToFormUrlEncoded = (body) =>
    Object.keys(body)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(body[key])
      )
      .join("&")

  const vercelRequestBody = vercelRequest.body
    ? JSON.parse(vercelRequest.body)
    : {}

  const { grant_type, redirect_uri, code, refresh_token } = vercelRequestBody

  const options = {
    hostname: "accounts.spotify.com",
    path: "/api/token",
    method: vercelRequest.method,
    headers: {
      Authorization: `Basic ${Buffer.from(
        `${CLIENT_ID}:${CLIENT_SECRET_ID}`,
        "binary"
      ).toString("base64")}`,
      "Content-Type": "application/x-www-form-urlencoded"
    }
  }

  const spotifyRequest = https.request(options, (spotifyResponse) => {
    spotifyResponse.on("data", (spotifyResponseData) => {
      vercelResponse.status(spotifyResponse.statusCode)
      vercelResponse.setHeader(
        "Access-Control-Allow-Origin",
        process.env.VERCEL_ENV === "development"
          ? "http://localhost:3000"
          : "https://neivaalf.gitlab.io"
      )

      vercelResponse.send(spotifyResponseData)
    })
  })

  spotifyRequest.end(
    objectToFormUrlEncoded({
      grant_type,
      redirect_uri,
      refresh_token,
      code
    })
  )
}
